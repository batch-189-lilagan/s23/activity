// Creating an object constructor
function Pokemon (name, level) {

    // properties
    this.name = name;
    this.level = level;
    this.health = 3 * level;
    this.attack = level;

    // Methods
    this.tackle = function(target) {
        
        let stillAlive = target.health > 0 ? true : false;
        let lastHit = (target.health - this.attack) <= 5 ? true : false;

        if (!stillAlive) {
            console.log(`💀 Attack Denied. ${target.name} is already dead!`);
        }

        else if (lastHit) {

            target.health = target.health - this.attack;
            console.log(`🎉 ${this.name} tackled and finished ${target.name}`);

            // Trigger Target's faint()
            target.faint();
        }

        else if (stillAlive) {
            target.health = target.health - this.attack;
            console.log(`🤜🏻 ${this.name} tackled (Attack: ${this.attack}) ${target.name}`);
            console.log(`🔋 ${target.name}'s health is now reduced from ${target.health + this.attack} to ${target.health}\n\n`);

        }
        
    }

    this.faint = function () {
        console.log(`😭 ${this.name} fainted.\n\n`);
    }

}

let pikachu = new Pokemon ('Pikachu', 10);
let mewtwo = new Pokemon ('Mewtwo', 20);

// console.log(pikachu);
// console.log(mewtwo);

// // pikachu.tackle(mewtwo);
// // pikachu.tackle(mewtwo);
// // pikachu.tackle(mewtwo);
// // pikachu.tackle(mewtwo);
// // pikachu.tackle(mewtwo);
// // pikachu.tackle(mewtwo);
// // pikachu.tackle(mewtwo);
// // pikachu.tackle(mewtwo);

// mewtwo.tackle(pikachu);

// pikachu.tackle(mewtwo);
// pikachu.tackle(mewtwo);

mewtwo.tackle(pikachu);
mewtwo.tackle(pikachu);
mewtwo.tackle(pikachu);
mewtwo.tackle(pikachu);
mewtwo.tackle(pikachu);
mewtwo.tackle(pikachu);
